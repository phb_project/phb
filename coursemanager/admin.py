from django.contrib import admin
from .models import Instructor, Course, Assignment, Document, Document_Update, Course_Document, Assignment_Document

# Register your models here.

admin.site.register(Instructor)
admin.site.register(Course)
admin.site.register(Assignment)
admin.site.register(Document)
admin.site.register(Document_Update)
admin.site.register(Course_Document)
admin.site.register(Assignment_Document)