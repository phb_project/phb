from django.shortcuts import get_object_or_404, render, HttpResponseRedirect, reverse
from .models import Instructor, Course, Assignment, Document, Document_Update, Course_Document, Assignment_Document
from .forms import AssignmentDetailsForm, ClassDetailsForm

def index(request):
    due_soon_list = Assignment.objects.order_by('due_date')[:5]
    class_list = Course.objects.all()
    semester_list = {}
    for c in class_list:
        if c.course_semester + ' ' + c.course_year not in semester_list:
            semester_list[c.course_semester + ' ' + c.course_year] = {}
            semester_list[c.course_semester + ' ' + c.course_year][c] = get_nearest_due(c.id)
        else:
            semester_list[c.course_semester + ' ' + c.course_year][c] = get_nearest_due(c.id)
            
    context = {
        'due_soon_list': due_soon_list,
        'class_list' : class_list,
        'semester_list': semester_list,
    }
    if request.method == 'POST':
        if Instructor.objects.first() is not None:
            temp_instructor = Instructor.objects.first() #save the actual instructor later based on inputs
        else:
            temp_instructor = Instructor(instructor_first_name="New", instructor_last_name="Instructor")
            temp_instructor.save()
        new_course = Course(course_semester="", instructor_id=temp_instructor)
        new_course.save()
        course = get_object_or_404(Course, pk=new_course.id)
        return HttpResponseRedirect(reverse('class_details', args=(course.id,)))
    else:    
        return render(request, 'coursemanager/index.html', context)
    
def assignment_details(request, assignment_id):
    assignment = get_object_or_404(Assignment, pk=assignment_id)
    if request.method == 'POST':
        save_assignment(request, assignment_id)
        return HttpResponseRedirect(reverse('index'))
    else:
        data = {'assignment_title' : assignment.assignment_title,
                'assignment_type' : assignment.assignment_type,
                'max_grade' : assignment.max_grade,
                'total_grade' : assignment.total_grade,
                'due_date' : assignment.due_date,
                'comments' : assignment.comments}
        form = AssignmentDetailsForm(data)
        form.populate_docs(assignment)
        return render(request, 'coursemanager/assignment_details.html', {'assignment': assignment, 'form': form})
    
def class_details(request, course_id):
    course = get_object_or_404(Course, pk=course_id)
    if request.method == 'POST':
        save_course(request, course_id)
        if 'newasgn' in request.POST:
            new_assignment = Assignment(course_id=course)
            new_assignment.save()
            assignment = get_object_or_404(Assignment, pk=new_assignment.id)
            return HttpResponseRedirect(reverse('assignment_details', args=(assignment.id,)))
        else:
            return HttpResponseRedirect(reverse('index'))
    else:
        data = {'course_title' : course.course_title,
                'course_semester' : course.course_semester,
                'course_credits' : course.course_credits,
                'course_year' : course.course_year,
                'final_grade' : course.final_grade,
                'course_level' : course.course_level,
                'course_department' : course.course_department,
                'course_number' : course.course_number,
                'instructor' : course.instructor_id}
        form = ClassDetailsForm(data)
        form.populate_docs(course)
        form.populate_assignments_grades(course)
        return render(request, 'coursemanager/class_details.html', {'course': course, 'form': form})
    
def save_assignment(request, assignment_id):
    assignment = get_object_or_404(Assignment, pk=assignment_id)
    assignment.assignment_title = request.POST['assignment_title']
    assignment.assignment_type = request.POST['assignment_type']
    assignment.max_grade = request.POST['max_grade']
    assignment.total_grade = request.POST['total_grade']
    assignment.due_date = request.POST['due_date']
    assignment.comments = request.POST['comments']
    if request.POST['document'] != '':
        newdoc = Document(file=request.POST['document'], document_type='Assignment')
        newdoc.save()
        newassignmentdoc = Assignment_Document(assignment_id=assignment, document_id=newdoc)
        newassignmentdoc.save()
        newdocu = Document_Update(file=request.POST['document'], change_notes=request.POST['document_comments'], document_id=newdoc)
        newdocu.save()
    assignment.save()
    
def save_course(request, course_id):
    course = get_object_or_404(Course, pk=course_id)
    course.course_title = request.POST['course_title']
    course.course_semester = request.POST['course_semester']
    course.course_credits = request.POST['course_credits']
    course.course_year = request.POST['course_year']
    course.final_grade = request.POST['final_grade']
    course.course_level = request.POST['course_level']
    course.course_department = request.POST['course_department']
    course.course_number = request.POST['course_number']

    instructor_name = request.POST['instructor']
    first,last = instructor_name.split(' ')
    if Instructor.objects.filter(instructor_last_name=last, instructor_first_name=first).exists():
        instructor = Instructor.objects.get(instructor_last_name=last, instructor_first_name=first)
    else:
        instructor = Instructor(instructor_last_name=last, instructor_first_name=first)
        instructor.save()
    
    course.instructor_id = instructor
    course.save()
    
    if request.POST['newsyl'] != '':
        newdoc = Document(file=request.POST['newsyl'], document_type='Syllabus')
        newdoc.save()
        newcoursedoc = Course_Document(course_id=course, document_id=newdoc)
        newcoursedoc.save()
        newdocu = Document_Update(file=request.POST['newsyl'], change_notes=request.POST['syl_comments'], document_id=newdoc)
        newdocu.save()
    if request.POST['newcrsdoc'] != '':
        newdoc = Document(file=request.POST['newcrsdoc'], document_type='Course')
        newdoc.save()
        newcoursedoc = Course_Document(course_id=course, document_id=newdoc)
        newcoursedoc.save()
        newdocu = Document_Update(file=request.POST['newcrsdoc'], change_notes=request.POST['crsdoc_comments'], document_id=newdoc)
        newdocu.save()
    
def get_nearest_due(crs_id):
    a = Assignment.objects.filter(course_id=crs_id).order_by('due_date').first()
    if a is not None:
        return "Next Assignment Due: " + str(a.due_date)
    else:
        return "Next Assignment Due: N/A"