from django.db import models
import datetime

# Create your models here.

aTypes = (
    ('P', 'Project'),
    ('E', 'Exam'),
    ('H', 'Homework'),
    ('L', 'Lab'),
    ('O', 'Other')
)

sTypes = (
    ('Spring', 'Spring'),
    ('Summer', 'Summer'),
    ('Fall', 'Fall'),
    ('Winter', 'Winter')
)

class Instructor(models.Model):
    instructor_last_name = models.CharField(max_length=50)
    instructor_first_name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.instructor_first_name + " " + self.instructor_last_name
    
class Course(models.Model):
    course_title = models.CharField(default='New Course', max_length=100)
    course_semester = models.CharField(max_length=6, choices=sTypes)
    course_credits = models.DecimalField(default=3.0, max_digits=2, decimal_places=1, blank=True)
    course_year = models.CharField(default=str(datetime.datetime.now().year), max_length=4)
    final_grade = models.DecimalField(default=0.0, max_digits=5, decimal_places=2, blank=True)
    course_level = models.CharField(max_length=4)
    course_department = models.CharField(max_length=3)
    course_number = models.CharField(max_length=3)
    instructor_id = models.ForeignKey(Instructor, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.course_title
    
class Assignment(models.Model):
    assignment_title = models.CharField(default='New Assignment', max_length=100)
    assignment_type = models.CharField(max_length=1, choices=aTypes)
    max_grade = models.DecimalField(default=0.0, max_digits=5, decimal_places=2, blank=True)
    total_grade = models.DecimalField(default=0.0, max_digits=5, decimal_places=2, blank=True)
    weight = models.DecimalField(default=0.0, max_digits=3, decimal_places=2, blank=True)
    due_date = models.DateTimeField('Due Date', default=datetime.datetime.now(), blank=True)
    comments = models.TextField(blank=True)
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.assignment_title
    
class Document(models.Model):
    file = models.CharField(max_length=100, blank=True)
    document_type = models.CharField(max_length=50, blank=True)
    
    def __str__(self):
        return self.file

class Document_Update(models.Model):
    file = models.CharField(max_length=100)
    change_notes = models.TextField(blank=True)
    document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.file
    
class Course_Document(models.Model):
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)
    document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.id)

class Assignment_Document(models.Model):
    assignment_id = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.id)
