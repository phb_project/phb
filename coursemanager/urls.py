from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<assignment_id>[0-9]+)/assignment_details/$', views.assignment_details, name='assignment_details'),
    url(r'^(?P<course_id>[0-9]+)/class_details/$', views.class_details, name='class_details'),
]