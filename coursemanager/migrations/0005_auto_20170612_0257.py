# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-12 08:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coursemanager', '0004_assignment_assignment_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='assignment_title',
            field=models.CharField(default='New Assignment', max_length=100),
        ),
    ]
