from django import forms
from .models import Assignment_Document, Document, Course_Document, Assignment, Document_Update
from decimal import Decimal

aTypes = (
    ('P', 'Project'),
    ('E', 'Exam'),
    ('H', 'Homework'),
    ('L', 'Lab'),
    ('O', 'Other')
)

sTypes = (
    ('Spring', 'Spring'),
    ('Summer', 'Summer'),
    ('Fall', 'Fall'),
    ('Winter', 'Winter')
)

lTypes = (
    ('1000', '1000'),
    ('2000', '2000'),
    ('3000', '3000'),
    ('4000', '4000')
)

class AssignmentDetailsForm(forms.Form):
    assignment_title = forms.CharField(label="Title", max_length=100)
    assignment_type = forms.ChoiceField(label="Type", choices=aTypes)
    max_grade = forms.DecimalField(label="Max Grade", max_digits=5, decimal_places=2, required=False)
    total_grade = forms.DecimalField(label="Total Grade", max_digits=5, decimal_places=2, required=False)
    weight = forms.DecimalField(label="Weight", max_digits=3, decimal_places=2, required=False)
    due_date = forms.DateTimeField(label="Due Date", required=False) #having trouble finding a working datetimepicker widget (need time too)
    comments = forms.CharField(label="Comments", widget=forms.Textarea, required=False)
    document = forms.FileField(label="Upload New Document", required=False)
    document_comments = forms.CharField(label="Document Note", required=False)
    
    def populate_docs(self, assgn_id):
        self.doclist = []
        for ad in Assignment_Document.objects.filter(assignment_id = assgn_id):
            for doculist in Document_Update.objects.filter(document_id=Document.objects.get(pk = (ad.document_id).id)):
                self.doclist.append({doculist.file : doculist.change_notes})
                
class ClassDetailsForm(forms.Form):
    course_title = forms.CharField(label="Title", max_length=100)
    course_semester = forms.ChoiceField(label="Semester", choices=sTypes)
    course_year = forms.CharField(label="Year", max_length=4, min_length=4)
    course_credits = forms.DecimalField(label="Credits", max_digits=2, decimal_places=1, required=False)
    final_grade = forms.DecimalField(label="Final Grade", max_digits=5, decimal_places=2, required=False)
    course_level = forms.ChoiceField(label="Course Level", choices=lTypes)
    course_department = forms.CharField(label="Course Department", max_length=3)
    course_number = forms.CharField(label="Course Number", max_length=3)
    instructor = forms.CharField(label="Instructor", max_length=101)
    
    def populate_docs(self, crs_id):
        self.doclist = []
        self.syllist = []
        for cd in Course_Document.objects.filter(course_id = crs_id):
            for doculist in Document_Update.objects.filter(document_id=Document.objects.get(pk = (cd.document_id).id)):
                if (Document.objects.get(pk = (doculist.document_id).id)).document_type == 'Syllabus':
                    self.syllist.append({doculist.file : doculist.change_notes})
                else:
                    self.doclist.append({doculist.file : doculist.change_notes})
                    
    def populate_assignments_grades(self, crs_id):
        self.asgnlist = []
        self.tent_grade = 0.0
        for a in Assignment.objects.all():
            if a.course_id == crs_id:
                self.asgnlist.append(a)
                self.tent_grade = Decimal(self.tent_grade) + Decimal((a.total_grade/a.max_grade)*a.weight)
        if self.tent_grade < 60:
            self.tent_grade = 'F'
        elif self.tent_grade < 70:
            self.tent_grade = 'D'
        elif self.tent_grade < 80:
            self.tent_grade = 'C'
        elif self.tent_grade < 90:
            self.tent_grade = 'B'
        else:
            self.tent_grade = 'A'
        
                